from .detector import TensoflowMobileNetSSDFaceDector


def _setup_detector() -> TensoflowMobileNetSSDFaceDector:
    return TensoflowMobileNetSSDFaceDector()


mobilenet_ssd_detector = _setup_detector()
