import logging

from aiohttp import web

from routes import setup_routes

logging.basicConfig(level="DEBUG",
                    format='%(asctime)-15s - %(levelname)s - %(message)s')
logger = logging.info(__name__)


def setup_app() -> web.Application:
    app = web.Application(logger=logger)
    return app


def main() -> None:
    app = setup_app()
    setup_routes(app)
    web.run_app(app)


if __name__ == '__main__':
    main()
