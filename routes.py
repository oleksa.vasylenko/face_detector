from aiohttp import web

from views import views


def setup_routes(app: web.Application) -> None:
    app.add_routes([
        web.get('/', views.status_handler),
        web.post('/detect', views.detect_handler)
    ])
