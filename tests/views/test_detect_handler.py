from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop

from constants import PATH_TEST_DATA
from main import setup_app
from views.views import detect_handler


class TestDetectHandler(AioHTTPTestCase):
    URL = "/detect"

    async def get_application(self):
        app = setup_app()
        app.router.add_post(self.URL, detect_handler)
        return app

    @unittest_run_loop
    async def test_detect_handler_one_face(self):
        image = open(f'{PATH_TEST_DATA}/one_face.jpg', 'rb')
        response = await self.client.request("POST", self.URL, data=image)

        expected_result = [[589, 113, 772, 345]]

        self.assertEqual(200, response.status)
        self.assertEqual(expected_result, await response.json())

    @unittest_run_loop
    async def test_detect_handler_no_face(self):
        image = open(f'{PATH_TEST_DATA}/no_face.jpg', 'rb')
        response = await self.client.request("POST", self.URL, data=image)

        expected_result = []

        self.assertEqual(200, response.status)
        self.assertEqual(expected_result, await response.json())

    @unittest_run_loop
    async def test_detect_handler_no_image(self):
        response = await self.client.request("POST", self.URL)

        expected_result = {'error': 'no image'}

        self.assertEqual(400, response.status)
        self.assertEqual(expected_result, await response.json())
