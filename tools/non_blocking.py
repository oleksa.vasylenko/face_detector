import asyncio

from functools import partial
from concurrent.futures import Executor
from typing import Optional, Any, Callable


async def run_async(func: Callable,
                    *args: Any,
                    pool: Optional[Executor] = None,
                    **kwargs: Any) -> Any:
    loop = asyncio.get_event_loop()
    return await loop.run_in_executor(pool, partial(func, *args, **kwargs))
