import cv2
import json
import numpy as np

from aiohttp import web

from detector import mobilenet_ssd_detector
from tools.non_blocking import run_async


async def status_handler(_: web.Request) -> web.Response:
    return web.Response(text="OK", content_type="text/html")


async def detect_handler(request: web.Request) -> web.Response:
    image = await request.read()
    if not image:
        resp_body = json.dumps({'error': 'no image'})
        return web.Response(status=400,
                            body=resp_body,
                            content_type="application/json")

    decoded = await run_async(cv2.imdecode,
                              np.frombuffer(image, np.uint8),
                              cv2.IMREAD_UNCHANGED)
    faces = await run_async(mobilenet_ssd_detector.detect_face, decoded)
    resp_body = json.dumps(faces.tolist())

    return web.Response(body=resp_body, content_type="application/json")
